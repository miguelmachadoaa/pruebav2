Pasos para instalar app 

Requiere php desde 8.1, Mysql, Composer, Node y npm.

#Como instalar xampp (Php y Mysql ) instalar la ultima version 

https://disenowebakus.net/xampp.php

#Como instalar composer

https://www.hostinger.es/tutoriales/como-instalar-composer

#Como instalar Node y Npm

https://kinsta.com/es/blog/como-instalar-node-js/


Una vez instaladas las apps.

1. Clonar rama master 

2 .- Ejecutar comando composer install 

3.- Renombrar archivo .env.example a .env 

4.- Crear base de datos mysql y cargar datos en .env en el campo DB_DATABASE

5.- Ejecutar comando composer install 

6.- ejecutar comando php artisan migrate --seed

7.- Ejecutar comando npm install

8.- ejecutar comando npm run dev (dejar esa ventana abierta )

9.- Ejecutar (en otra ventana) comando php artisan serve.

10 - Para ejecutar pruebas unitarias debe crear una carpeta llamada Unit dentro de tests (no se porque no subio ) y  ejecutar php artisan test, se verifica el registro.

La app cuenta con un usuario admin con las siguientes credenciales 

usuario admin@gmail.com
pass 123456

Los usuario que se registren quedan inactivos para activarlos debes ingresar a la app ir al menu usuarios y darle al boton activar, de esta forma podran acceder a la app.

En el menu post se pueden crear y eliminar publicaciones con el usuario admin, con los demas solo crear.
