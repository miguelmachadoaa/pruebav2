<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', [HomeController::class, 'index'])->name('home.index');
Route::get('/send', [HomeController::class, 'create'])->name('home.create');
Route::get('/getpost', [HomeController::class, 'getpost'])->name('home.getpost');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {

    Route::resource('post', PostController::class);

    Route::get('users/', [UserController::class, 'index'])->name('users.index');

    Route::get('users/{id}/activar', [UserController::class, 'activar'])->name('users.activar');
    Route::get('users/{id}/desactivar', [UserController::class, 'desactivar'])->name('users.desactivar');
    

    Route::post('post/store', [PostController::class, 'store'])->name('post.store');

    Route::post('post/{id}/update', [PostController::class, 'update'])->name('post.actualizar');
    Route::post('users/{id}/update', [UserController::class, 'update'])->name('users.actualizar');


    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
