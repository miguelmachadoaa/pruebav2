<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
   /* public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }*/

     public function test_carga_registro(): void
    {
        $response = $this->get('/register');

        $response->assertStatus(200);
    }


    public function test_falla_registro_menor_18(): void
    {
        $response = $this->post('/register', [
            'name' => 'Test User',
            'email' => time().'@example.com',
            'role' => '1',
            'estado' => '1',
            'dob' => '2023-01-01',
            'password' => 'password',
            'password_confirmation' => 'password',
        ]);

       # $this->assertAuthenticated();
        $response->assertRedirect('/register');
    }


    public function test_exito_registro_mayor_18(): void
    {
        $response = $this->post('/register', [
            'name' => 'Test User',
            'email' => time().'@example.com',
            'role' => '1',
            'estado' => '1',
            'dob' => '2000-01-01',
            'password' => 'password',
            'password_confirmation' => 'password',
        ]);

        #$this->assertAuthenticated();
        $response->assertRedirect('/login');
    }


}
