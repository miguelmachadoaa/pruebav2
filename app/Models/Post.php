<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

     public $table = 'post';

     public $fillable = [
        'id',
        'titulo',
        'descripcion',
        'estado',
        'id_user'
    ];
}
