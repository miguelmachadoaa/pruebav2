<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
    {
        
        $users = User::get();

        return view('users.index', compact('users'));

    }


     public function activar($id)
    {
         $user= User::find($id);

         if ($user->id) {
             $user->update(['estado'=>1]);
         }

         return redirect('/users')->with('success', 'Registro Actualizado'); 
    }


     public function desactivar($id)
    {
         $user= User::find($id);

         if ($user->id) {
             $user->update(['estado'=>0]);
         }

         return redirect('/users')->with('success', 'Registro Actualizado'); 
    }

   
}
