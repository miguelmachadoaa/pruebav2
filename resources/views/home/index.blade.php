<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Listadp de Publicaciones</title>

        <!-- Fonts -->
        <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            
        </style>

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>

        <script src="https://js.pusher.com/7.2/pusher.min.js"></script>
          <script>

            // Enable pusher logging - don't include this in production
            Pusher.logToConsole = true;

            var pusher = new Pusher('ecd7e86aa1c2c7248443', {
              cluster: 'us2'
            });

            var channel = pusher.subscribe('my-channel');
            channel.bind('my-event', function(data) {
                 //alert(JSON.stringify(data))
                fetch('{{url('/getpost')}}')
                  .then((response) => response.text())
                  .then((data) =>  document.querySelector(".listadopublicaciones").innerHTML=data );

            });


        </script>


        @vite(['resources/css/app.css', 'resources/js/app.js'])
    </head>
    <body class="antialiased">
        <div class="container mx-auto">
            <div class="">
                

            </div>

            <header>
    <nav class="bg-white border-gray-200 px-4 lg:px-6 py-2.5 ">

        <div class="flex flex-wrap justify-between items-center mx-auto max-w-screen-xl">
            <a href="https://flowbite.com" class="flex items-center">
               
                <span class="self-center text-xl font-semibold whitespace-nowrap ">Dentos</span>
            </a>

            @if (Route::has('login'))
                        @auth
                           

                            <a href="{{ url('/dashboard') }}" class="text-gray-800  hover:bg-gray-50 focus:ring-4 focus:ring-gray-300 font-medium rounded-lg text-sm px-4 lg:px-5 py-2 lg:py-2.5 mr-2  focus:outline-none ">Dashboard</a>
                        @else
                            

                             <a href="{{ route('login') }}" class="text-gray-800  hover:bg-gray-50 focus:ring-4 focus:ring-gray-300 font-medium rounded-lg text-sm px-4 lg:px-5 py-2 lg:py-2.5 mr-2  focus:outline-none ">Log in</a>

                            @if (Route::has('register'))
                             <a href="{{ route('register') }}" class="text-gray-800  hover:bg-gray-50 focus:ring-4 focus:ring-gray-300 font-medium rounded-lg text-sm px-4 lg:px-5 py-2 lg:py-2.5 mr-2  focus:outline-none ">Register</a>
                                
                            @endif
                        @endauth
                @endif
           
           
        </div>
    </nav>
</header>


            <section class="bg-white mt-4 mb-4">
              <div class="py-8 px-4 mx-auto max-w-screen-xl lg:py-16 lg:px-6">
                   <div class="mx-auto max-w-screen-sm text-center mb-8 lg:mb-16">
                          <h1 class="mb-4 text-4xl tracking-tight font-extrabold text-gray-900 ">Nuestro Blog</h1>
                          <p class="mb-4 text-4xl tracking-tight font-extrabold text-gray-900 ">Listado de entradas</p>
          
                    </div> 


                    <div class="mx-auto max-w-screen-sm text-center mb-8 lg:mb-16">

                        <form method="get" action="{{url('/')}}">   
                            <label for="default-search" class="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white">Buscar por fecha</label>
                            <div class="relative">
                                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path></svg>
                                </div>

                                <input type="date" id="fecha" name="fecha" class="block w-full p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 " placeholder="Buscar por fecha"  value="{{$fecha}}" required>

                                <button type="submit" class="text-white absolute right-2.5 bottom-2.5 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 ">Buscar </button>
                            </div>
                        </form>

          
                    </div> 
                  <div class="grid gap-8 lg:grid-cols-2 listadopublicaciones">


                     @foreach($post as $p)
                          
                        </article>

                            <article class="p-6 bg-white rounded-lg border border-gray-200 shadow-md">
                              <div class="flex justify-between items-center mb-5 text-gray-500">
                                 
                                  <span class="text-sm">{{$p->created_at}}</span>
                              </div>
                              <h2 class="mb-4 mt-4 text-2xl font-bold tracking-tight text-gray-900 "><a href="#">{{$p->titulo}}</a></h2>

                              <p class="mb-5 font-light text-gray-500 dark:text-gray-400">{{$p->descripcion}}</p>
                          
                        </article> 


                    @endforeach

                                   
                  </div>  
              </div>
            </section>





        </div>
    </body>
</html>
