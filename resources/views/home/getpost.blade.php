@foreach($post as $p)
                          
    </article>

        <article class="p-6 bg-white rounded-lg border border-gray-200 shadow-md">
          <div class="flex justify-between items-center mb-5 text-gray-500">
             
              <span class="text-sm">{{$p->created_at}}</span>
          </div>
          <h2 class="mb-4 mt-4 text-2xl font-bold tracking-tight text-gray-900 "><a href="#">{{$p->titulo}}</a></h2>

          <p class="mb-5 font-light text-gray-500 dark:text-gray-400">{{$p->descripcion}}</p>
      
    </article> 


@endforeach