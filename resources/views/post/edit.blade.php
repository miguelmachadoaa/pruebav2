<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Editar Post
        </h2>
    </x-slot>

    <div class="py-12 mx-12">

        <div class="mx-12">
            
            <form method="POST" action="{{url('post/'.$post->id.'/update')}}">

                <div class="grid gap-6 mb-6 md:grid-cols-1">

                    <div>
                        <label for="first_name" class="block mb-2 text-sm font-medium text-gray-900 ">Titulo</label>

                        <input type="text" id="titulo" name="titulo" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 " placeholder="Titulo" value="{{$post->titulo}}" required>

                    </div>

                    <div>
                        <label for="first_name" class="block mb-2 text-sm font-medium text-gray-900 ">Descripción</label>

                        <textarea id="descripcion" name="descripcion" rows="10" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 " placeholder="Titulo" required>{{$post->descripcion}}</textarea>


                    </div>
                  
                <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm  sm:w-auto px-5 py-2.5 text-center ">Editar</button>
            </form>

        </div>
        


    </div>
</x-app-layout>
