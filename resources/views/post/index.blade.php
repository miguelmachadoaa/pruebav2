<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Post
        </h2>
    </x-slot>

    <div class="py-12 mx-12">

        <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
    <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
        <caption class="p-5 text-lg font-semibold text-left text-gray-900 bg-white ">
            Nuestras publicaciones
            
        </caption>
        <thead class="text-xs text-gray-700 uppercase bg-gray-50 ">
            <tr>
                <th scope="col" class="px-6 py-3">
                    Titulo
                </th>
                <th scope="col" class="px-6 py-3">
                    Descripcion
                </th>
                <th scope="col" class="px-6 py-3">
                    Creado
                </th>
                
                <th scope="col" class="px-6 py-3">
                     <a href="{{url('post/create')}}" class="font-medium text-blue-600  hover:underline">Crear</a>
                </th>
            </tr>
        </thead>
        
        <tbody>

            @foreach($posts as $p )
            <tr class="bg-white border-b ">
                <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap ">
                    {{$p->titulo}}
                </th>
                <td class="px-6 py-4">
                    {{$p->descripcion}}
                </td>
                <td class="px-6 py-4">
                    {{$p->created_at}}
                </td>
               
                <td class="px-6 py-4 text-right">
                    @if( Auth::user()->role=='1')
                    <a href="{{url('post/'.$p->id.'/edit')}}" class="font-medium text-blue-600  hover:underline">Edit</a>
                    @endif
                </td>
            </tr>
            @endforeach

            
        </tbody>
    </table>
</div>
        
    </div>
</x-app-layout>
