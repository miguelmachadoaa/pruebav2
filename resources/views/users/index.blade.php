<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Post
        </h2>
    </x-slot>

    <div class="py-12 mx-12">

        <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
    <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
        <caption class="p-5 text-lg font-semibold text-left text-gray-900 bg-white ">
            Nuestras publicaciones
            
        </caption>
        <thead class="text-xs text-gray-700 uppercase bg-gray-50 ">
            <tr>
                <th scope="col" class="px-6 py-3">
                    Nombre
                </th>
                <th scope="col" class="px-6 py-3">
                    Email
                </th>
                <th scope="col" class="px-6 py-3">
                    Creado
                </th>
                
                <th scope="col" class="px-6 py-3">
                     Acciones
                </th>
            </tr>
        </thead>
        
        <tbody>

           @foreach($users as $u )
            <tr class="bg-white border-b ">
                <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap ">
                    {{$u->name}}
                </th>
                <td class="px-6 py-4">
                    {{$u->email}}
                </td>
                <td class="px-6 py-4">
                    {{$u->created_at}}
                </td>
               
                <td class="px-6 py-4 text-right">
                    @if($u->estado == '0')
                    
                        <a href="{{url('users/'.$u->id.'/activar')}}" class="text-white bg-red-700 hover:bg-red-800 focus:outline-none focus:ring-4 focus:ring-red-300 font-medium rounded-full text-sm px-5 py-2.5 text-center mr-2 mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900">Desactivado</a>

                    @else
                        <a href="{{url('users/'.$u->id.'/desactivar')}}" class="text-white bg-green-700 hover:bg-green-800 focus:outline-none focus:ring-4 focus:ring-green-300 font-medium rounded-full text-sm px-5 py-2.5 text-center mr-2 mb-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800">Activado</a>
                    @endif
                </td>
            </tr>
            @endforeach

            
        </tbody>
    </table>
</div>
        
    </div>
</x-app-layout>
